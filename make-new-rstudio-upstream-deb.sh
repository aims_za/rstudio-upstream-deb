#!/bin/bash

# First place the new upstream binaries in ~/rstudio-upstream-deb/upstream,
# and remove the old ones. This script made for 12.04 in Jan's account

VERSION=`ls /home/jan/rstudio-upstream-deb/upstream|cut -d\- -f2,2|head -1`ppa1
echo "VERSION="${VERSION}
cd /home/jan/rstudio-upstream-deb

# Put upstream files in correct filenames and preferred format for debian package
echo "tar -cvzf rstudio-upstream-deb_${VERSION}.orig.tar.gz --files-from /dev/null"
tar -cvzf rstudio-upstream-deb_${VERSION}.orig.tar.gz --files-from /dev/null
echo "cd rstudio-upstream-deb/"
cd rstudio-upstream-deb/
echo "rm -rf amd64/"
rm -rf amd64/
echo "dpkg -x /home/jan/rstudio-upstream-deb/upstream/rstudio-${VERSION}-amd64.deb amd64/"
dpkg -x /home/jan/rstudio-upstream-deb/upstream/rstudio-${VERSION}-amd64.deb amd64/
echo "rm -rf i386/"
rm -rf i386/
echo "dpkg -x /home/jan/rstudio-upstream-deb/upstream/rstudio-${VERSION}-i386.deb i386/"
dpkg -x /home/jan/rstudio-upstream-deb/upstream/rstudio-${VERSION}-i386.deb i386/
echo "tar -czf ../rstudio-upstream-deb_${VERSION}.orig-amd64.tar.gz amd64/"
tar -czf ../rstudio-upstream-deb_${VERSION}.orig-amd64.tar.gz amd64/
echo "tar -czf ../rstudio-upstream-deb_${VERSION}.orig-i386.tar.gz i386/"
tar -czf ../rstudio-upstream-deb_${VERSION}.orig-i386.tar.gz i386/

# make changes to debian/* if needed

##debchange -i ## replaced by below non-interactive
cd debian
cat > changelog.new << EOF
rstudio-upstream-deb (${VERSION}) trusty; urgency=low

  * New upstream release (${VERSION})

EOF
echo " -- Jan Groenewald <jan@aims.ac.za>  `date +\"%a, %d %b %Y %T %z\"`" >> changelog.new
echo >> changelog.new
cat changelog >> changelog.new
mv changelog.new changelog
cd ..

# Build the debs, unsigned
debuild --no-lintian -b -us -uc
debuild --no-lintian -ai386 -b -us -uc
# Build the source, unsigned
debuild --no-lintian -S -us -uc
# Sign the packages (interactive for the user), dsc and source.changes
#debsign -k'Jan Groenewald (Www.aims.ac.za) <jan@aims.ac.za>'
echo "debsign -k'Jan Groenewald (Www.aims.ac.za) <jan@aims.ac.za>' -S"
debsign -k'Jan Groenewald (Www.aims.ac.za) <jan@aims.ac.za>' -S
# Upload the source package
echo "Test the package!"
echo "unset ftp_proxy; dput ppa:aims/aims-desktop-dev rstudio-upstream-deb_${VERSION}_source.changes"

